package de.maficraft.headhunterz;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Dient als Event Listener, wenn ein Spieler zum Mörder wird, ein Mörder getötet
 * wurde, ein Kopfgeld auf jemanden ausgesetzt wurde, usw.
 * @author Morph <admin@mds-tv.de>
 */
public interface MurdererListener {
    /**
     * Wird gefeuert, wenn ein Spieler zum Mörder wird.
     * @param murderer Das Murderer Objekt des Spielers
     */
    public void onPlayerGetsMurderer(Murderer murderer);
    
    /**
     * Wird gefeuert, wenn ein Mörder einen Spieler angreift
     * @param murderer Der Mörder
     * @param player Der angegriffene Spieler
     * @param eventData Enthält Event Daten über den Angriff
     */
    public void onMurdererAttacksPlayer(Murderer murderer, Player player, EntityDamageByEntityEvent eventData);
    
    /**
     * Wird gefeuert, wenn ein Mörder einen Spieler tötet
     * @param murderer Der Mörder
     * @param player Der getötete Spieler
     */
    public void onMurdererKilledPlayer(Murderer murderer, Player player);
    
    /**
     * Wird gefeuert, wenn ein Mörder getötet wird.
     * @param murderer Der getötete Mörder
     */
    public void onMurdererKilled(Murderer murderer);
    
    /**
     * Wird gefeuert, wenn ein Spieler Kopfgeld auf einen Mörder aussetzt.
     * @param murderer Der Mörder, auf den das Kopfgeld ausgesetzt wird
     * @param bounty Die Höhe des ausgesetzten Kopfgeldes
     */
    public void onBountyAdded(Murderer murderer, double bounty);
    
    /**
     * Wird gefeuert, wenn sich ein Mörder auf den Server connected.
     * @param murderer Der Mörder
     */
    public void onMurdererConnected(Murderer murderer);
    
    /**
     * Wird gefeuert, wenn ein Mörder die Verbindung zum Server trennt.
     * @param murderer Der Mörder
     */
    public void onMurdererDisconnected(Murderer murderer);
}
