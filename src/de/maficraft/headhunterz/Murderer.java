package de.maficraft.headhunterz;

import java.util.HashMap;
import java.util.Map;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;

/**
 * Hält Informationen über einen Mörder und bietet Methoden, um diesen zu
 * verwalten.
 * @author Morph <admin@mds-tv.de>
 */
public class Murderer {
    /**
     * Beschreibt die Rückgaben der addBounty Methode.
     */
    public enum BountyResponse {
        /**
         * Das Kopfgeld wurde erfolgreich ausgesetzt
         */
        SUCCESS,
        
        /**
         * Der Spieler hat nicht genügend Geld zur Verfügung
         */
        NO_MORE_MONEY,
        
        /**
         * Das Kopfgeld ist zu niedrig
         */
        BOUNTY_TOO_LOW,
        
        /**
         * Das Kopfgeld ist zu hoch
         */
        BOUNTY_TOO_HIGH;
    }
    
    /**
     * Referenz zum HeadhunterZ Plugin
     */
    private final HeadhunterZ pluginInstance;
    
    /**
     * Referenz zum Spieler Objekt.
     */
    private final Player killer;
    
    /**
     * Das Kopfgeld, dass auf diesen Spieler ausgesetzt ist.
     */
    private final HashMap<Player, Double> bounties = new HashMap<>();
    
    /**
     * Der Zeitpunkt, wann der Spieler seinen Status als Mörder verliert.
     * Jeder Kill setzt diese Zeit höher!
     */
    private long murdererReleaseTimestamp;
    
    /**
     * Referenz zum Economy System
     */
    private Economy eco;
    
    /**
     * Liste der getöteten Spieler, seit dieser Spieler als Mörder bekannt ist.
     */
    private final HashMap<Player, Long> killed = new HashMap<>();

    /**
     * Erzeugt ein neues Info Pack
     * @param pluginInstance Die Referenz zum HeadhunterZ Plugin
     * @param killer Der Mörder, dem dieses Info Pack zugeordnet ist
     */
    public Murderer(HeadhunterZ pluginInstance, Player killer) {
        // Die Plugin Referenz darf nicht null sein.
        if (pluginInstance == null) {
            throw new NullPointerException("Plugin Instance cannot be null!");
        }
        
        // Die Player Referenz darf nicht null sein.
        if (killer == null) {
            throw new NullPointerException("To create a murderer you need a valid killer and the first killed player.");
        }
        
        this.killer = killer;
        this.pluginInstance = pluginInstance;
        this.eco = pluginInstance.getEconomy();
    }

    /**
     * Gibt das Player Objekt des Mörders zurück
     * @return Das Player Objekt des Mörders
     */
    public Player getPlayer() {
        return killer;
    }
    
    /**
     * Gibt die Map der getöteten Spieler zurück + den Zeitpunkt, wann diese
     * getötet wurden.
     * @return Map der getöteten Spieler.
     */
    public Map<Player, Long> getKilled() {
        return this.killed;
    }

    /**
     * Gibt das aktuelle Kopfgeld zurück, welches auf diesen Mörder ausgesetzt ist.
     * @return Das aktuell Kopfgeld.
     */
    public double getEntireBounty() {
        double entireBounty = 0.0D;
        
        // Zusammenzählen der ausgesetzten Kopfgelder
        for (double bounty : this.bounties.values()) {
            entireBounty += bounty;
        }
        
        // Zurückgeben des gesamten Kopfgeldes
        return entireBounty;
    }
    
    /**
     * Gibt das Kopfgeld zurück, welches der übergebene Spieler ausgesetzt hat.
     * Wenn der Spieler kein Kopfgeld ausgesetzt hat, wird 0.0 zurück gegeben.
     * @param player Der Spieler, dessen ausgesetztes Kopfgeld abgefragt werden soll
     * @return Das ausgesetzte Kopfgeld
     */
    public double getBountyForPlayer(Player player) {
        Double bounty = this.bounties.get(player);
        return bounty != null ? bounty : 0.0D;
    }
    
    /**
     * Gibt die ausgesetzten Kopfgelder zurück.
     * @return Die ausgesetzten Kopfgelder
     */
    public HashMap<Player, Double> getBounties() {
        return this.bounties;
    }
    
    /**
     * Setzt das Kopfgeld für diesen Mörder um den angegebenen Betrag.
     * @param player Der Spieler, der das Kopfgeld aussetzt.
     * @param bounty Das hinzuzufügende Kopfgeld.
     * @return Das Ergbenis als BountyResponse
     */
    // TODO Diese Methode muss EINGEHEND getestet werden!!!
    public BountyResponse addBounty(Player player, double bounty) {
        double minBounty = this.pluginInstance.getConfig().getDouble("headhunterz.min_bounty", 0.0D);
        double maxBounty = this.pluginInstance.getConfig().getDouble("headhunterz.max_bounty", 0.0D);
        
        // Hier schauen wir, ob das Kopfgeld auch die gegebenen
        // Bedingungen erfüllt.
        if (bounty > 0.0  && bounty >= minBounty) {
            // Hat der Spieler bereits ein Kopfgeld ausgesetzt?
            // Wenn ja, dann verrechnen wir das Alte und das Neue miteinander.
            Double prevObj  = bounties.get(player);
            double previous =  prevObj == null ? 0 : prevObj.doubleValue();
            
            // Die Differenz zwischen dem Alten und dem Neuen Kopfgeld
            double calcBounty = bounty - previous;
            
            // Wenn die Differenz negativ ist, war das alte Kopfgeld höher. Der
            // Spieler bekommt die Summe der Differenz zurück.
            if (calcBounty < 0.0D) {
                this.eco.bankDeposit(player.getName(), calcBounty + previous);
            }
            
            // Nun prüfen wir, ob der Spieler auf genügend Geld zur Verfügung hat
            if (this.eco.has(player.getName(), calcBounty)) {
                bounties.put(player, bounty);
                this.eco.withdrawPlayer(player.getName(), calcBounty);
                
                // EVENT: onBountyAdded
                for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                    l.onBountyAdded(this, bounty);
                }

                // Wenn alles geklappt hat, geben wir SUCCESS zurück
                return BountyResponse.SUCCESS;
            } else {
                // Wenn der Spieler nicht genügend Geld hat, geben wir NO_MORE_MONEY zurück.
                return BountyResponse.NO_MORE_MONEY;
            }
        } else {
            return BountyResponse.BOUNTY_TOO_LOW;
        }
    }
    
    /**
     * Nimmt das vom übergebenen Spieler ausgesetzte Kopfgeld wieder zurück.
     * Der Spieler erhält das Geld zurück.
     * @param player Der Spieler, dessen Kopfgeld zurück genommen werden soll.
     */
    public void retractBounty(Player player) {
        double bounty = getBountyForPlayer(player);
        
        // Wenn der Spieler ein Kopfgeld ausgesetzt hat wird dieses zurück
        // erstattet und der Spieler wird aus der Liste genommen.
        if (bounty > 0.0D) {
            this.eco.depositPlayer(player.getName(), bounty);
            this.bounties.remove(player);
        }
    }

    /**
     * Gibt zurück, wann der Spieler nicht länger als Mörder gilt.
     * @return Unix Timestamp wann der Spieler nicht länger als Mörder gilt.
     */
    public long getTime() {
        return murdererReleaseTimestamp;
    }

    /**
     * Setzt den Timeout als Unix Timestamp, wann der Spieler nicht mehr als Mörder
     * gilt. Die Zeit wird als Sekunden gewertet.
     * 
     * @param time Zeitpunkt des Releases
     */
    public void setMurdererReleaseTimestamp(long time) {
        if (time > 0) {
            this.murdererReleaseTimestamp = time * 1000;
        }
    }
    
    /**
     * Erhöht die Zeit, wie lange der Spieler noch als Mörder gilt.
     * @param time Die zu addierende Zeit in Sekunden.
     */
    public void addTime(long time) {
        if (time > 0) {
            this.murdererReleaseTimestamp += time * 1000;
        }
    }
}
