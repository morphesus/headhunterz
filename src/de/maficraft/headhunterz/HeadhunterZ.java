package de.maficraft.headhunterz;

import de.maficraft.headhunterz.commands.CommandDispatcher;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * HeadhunterZ Hauptklasse
 * @author Morph <admin@mds-tv.de>
 */
public class HeadhunterZ extends JavaPlugin {
    /**
     * Erzeugt den Kill Event Dispatcher, der für das Verarbeiten der Kills
     * verantwortlich ist.
     */
    private KillEventDispatcher killEventDispatcher;
    
    /**
     * Liste an registrierten Listenern.
     */
    private final List<MurdererListener> mListeners = new ArrayList<>();
    
    /**
     * Die Konfiguration für die Sprache.
     */
    private YamlConfiguration localeConfig;
    
    /**
     * Die Standartkonfiguration für die Sprache.
     * Sie kommt zum Einsatz, wenn die locale.yml nicht alle Übersetzungen
     * enthält.
     */
    private YamlConfiguration defaultLocaleConfig;
    
    /**
     * Der Economy Provider stellt die Brücke zum Economy System bereit.
     */
    private Economy economy;
    
    /**
     * Der Command Dispatcher verwaltet die Commands des Plugins
     */
    private CommandDispatcher cmdDispatcher;
    
    @Override
    public void onEnable() {
        // Aufrufen der onEnable Methode von Bukkit
        super.onEnable();
        
        // Konfiguration prüfen & erzeugen, wenn nicht vorhanden
        this.checkConfig();
        
        // Initialisiert den Kill Listener
        this.killEventDispatcher = new KillEventDispatcher(this);
        this.getServer().getPluginManager().registerEvents(killEventDispatcher, this);
        
        // Registriert den Master-Command
        this.cmdDispatcher = new CommandDispatcher(this);
        this.getServer().getPluginCommand("hh").setExecutor(this.cmdDispatcher);
        
        // Jetzt können wir die Sub-Commands laden
        this.loadCommands();
    }
    
    /**
     * Prüft, ob die Plugin Konfiguration existiert. Sofern die Konfiguration
     * nicht existiert wird sie angelegt.
     */
    private void checkConfig() {
        // Wenn das Pluginverzeichnis nicht existiert wird es angelegt
        if (!getDataFolder().isDirectory()) {
            getLogger().info("Plugin Datafolder not found ... Creating.");
            getDataFolder().mkdir();
        }
        
        // Kopieren der Default Konfiguration aus der Plugin JAR ins Plugin Verzeichnis.
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.isFile()) {
            try {
                getLogger().info("Plugin Configuration not found ... Creating.");
                
                InputStream is = getClass().getResourceAsStream("/config_example.yml");
                Files.copy(is, Paths.get(configFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                getLogger().log(Level.SEVERE, "Could not create default Plugin Configuration!", ex);
            }
        }
        
        // Kopieren der default Locale, wenn diese nicht existiert
        File localeFile = new File(getDataFolder(), "locale.yml");
        if (!localeFile.isFile()) {
            try {
                getLogger().info("Plugin Locale not found ... Creating.");
                
                InputStream is = getClass().getResourceAsStream("/locale.yml");
                Files.copy(is, Paths.get(localeFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                getLogger().log(Level.SEVERE, "Could not create default locale!", ex);
            }
        }
        
        getLogger().info("Loading locale ...");
        
        // Laden der Sprachkonfiguration
        this.localeConfig = YamlConfiguration.loadConfiguration(localeFile);
        
        getLogger().log(Level.INFO, "Loaded locale: {0}", this.localeConfig.get("locale.locale_name", "Unknown"));
        
        // Laden der default Sprachkonfiguration
        this.defaultLocaleConfig = YamlConfiguration.loadConfiguration(getClass().getResourceAsStream("/locale.yml"));
        
        getLogger().log(Level.INFO, "Loaded fallback locale: {0}", this.defaultLocaleConfig.get("locale.locale_name"));
    }
    
    /**
     * Initialisiert den Vault Provider
     */
    private void initVault() {
        // Zuerst versuchen wir uns den Economy Service zu holen ...
        RegisteredServiceProvider<Economy> ecoProvider = getServer().getServicesManager().getRegistration(Economy.class);
        
        // ... dann schauen wir, ob er auch vorhanden ist.
        // Sofern er das ist, laden wir die Verbindung zum Economy System.
        if (ecoProvider != null) {
            this.economy = ecoProvider.getProvider();
            
            getLogger().log(Level.INFO, "Successfully loaded Economy Provider: {0}", this.economy.getName());
        } else {
            getLogger().severe("Could not load economy provider! Plugin will disabled.");
            
            this.setEnabled(false);
        }
    }
    
    /**
     * Läd die in der config.yml angegebenen Commands.
     * Die folgende Syntax zum Registrieren von Commands sollte in der config.yml
     * berücksichtigt werden:
     * 
     * subcommands:
     *     - <cmd_label>=<cmd_class>
     * 
     * Hier ein kleines Beispiel:
     * 
     * subcommands:
     *     - list=de.maficraft.commands.ListCommand
     */
    private void loadCommands() {
        // Laden der subcommands Liste aus der config.yml
        List<String> subCommandClasses = this.getConfig().getStringList("headhunterz.subcommands");
        
        // Iterieren der Einträge
        for (String subCommandClass : subCommandClasses) {
            try {
                // Splitten des Eintrags zum Label und dem Klassenpfad
                String[] cmd = subCommandClass.split("=");
                
                // Prüfen, ob auch beides vorhanden ist
                if (cmd.length == 2) {
                    String cmdLabel = cmd[0];
                    String cmdClass = cmd[1];
                    
                    // Laden und registrieren der Klasse
                    Class<?> cmdClassObject = Class.forName(cmdClass);
                    List<Class<?>> cmdClassInterfaces = Arrays.asList(cmdClassObject.getInterfaces());
                    if (cmdClassInterfaces.contains(CommandExecutor.class)) {
                        this.cmdDispatcher.registerCommand(subCommandClass, (Class<CommandExecutor>) cmdClassObject);
                        this.getLogger().log(Level.INFO, "Loaded Subcommand: {0}", cmdLabel);
                    }
                    // Die Klasse implementiert den CommandExecutor nicht
                    else {
                        this.getLogger().log(Level.SEVERE, "Invalid Command Class: {0}", subCommandClass);
                    }
                }
                // Es liegt ein Syntaxfehler in der Definition eines Commands vor
                else {
                    this.getLogger().severe("Invalid command syntax in config.yml");
                }
            }
            // Die angegebene Command Klasse konnte nicht gefunden werden
            catch (ClassNotFoundException ex) {
                this.getLogger().log(Level.SEVERE, "Cannot load command: " + subCommandClass, ex);
            }
        }
    }
    
    /**
     * Gibt die Verbindung zum Economy System zurück.
     * @return Verbindung zum Economy System.
     */
    public Economy getEconomy() {
        return this.economy;
    }
    
    /**
     * Registriert einen MurderListener, mit welchem die HeadhunterZ Events
     * abgefangen werden können.
     * @param listener Der zu registrierende Listener.
     */
    public void registerMurderListener(MurdererListener listener) {
        // Der Listener darf nicht null sein
        if (listener == null) {
            throw new NullPointerException("Cannot add null as listener");
        }
        
        // Registriert den Listener
        this.mListeners.add(listener);
    }
    
    /**
     * Gibt die Liste der aktuell registrierten Listeners zurück.
     * @return Liste der aktuell registrierten Listeners.
     */
    public List<MurdererListener> getRegisteredListeners() {
        return this.mListeners;
    }
}
