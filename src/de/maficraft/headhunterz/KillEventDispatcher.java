package de.maficraft.headhunterz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Verarbeitet die Kill Events.
 * @author Morph <admin@mds-tv.de>
 */
public class KillEventDispatcher implements Listener {
    private final HeadhunterZ pluginInstance;
    
    /**
     * Liste der Mörder.
     */
    private final List<Murderer> murderers = new ArrayList<>();
    
    /**
     * Dieser Thread verwaltet die Informationen über die Mörder.
     * Jeder Spieler kann nur auf bestimmte Zeit Mörder sein, diese wird in der
     * Konfiguration des Plugins festgelegt.
     * 
     * Solange der Mörder keine weiteren Spieler tötet wird die Zeitspanne nicht
     * verlängert. Wenn das Ende dieser Zeitspanne erreicht ist verliert der
     * Spieler seinen Status als Mörder.
     */
    private Thread killManager;

    /**
     * Initialisiert einen neuen KillDispatcher
     * 
     * @param plugin Referenz zum HeadhunterZ Plugin
     */
    public KillEventDispatcher(HeadhunterZ plugin) {
        pluginInstance = plugin;
        
        // Initialisierung des Kill Manager Threads
        initKillManagerThread();
    }
    
    /**
     * Initialisiert den KillManager Thread
     */
    private void initKillManagerThread() {
        // Erzeugen des Manager Threads
        killManager = new Thread(new Runnable() {
            @Override
            public void run() {
                pluginInstance.getLogger().info("Starting KILL-MANAGER Thread");
                
                while (pluginInstance.isEnabled()) {
                    // Aufräumen der Mörder Liste durchführen
                    cleanUpMurderersList();
                    
                    // Damit der Thread nicht die CPU frisst wird er etwas
                    // ausgebremst.
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        pluginInstance.getLogger().log(Level.SEVERE, null, ex);
                    }
                }
            }
        }, "KILL-MANAGER");
        
        // Starten des Threads
        killManager.start();
    }
    
    /**
     * Räumt die Liste der aktuellen Mörder auf. Spieler, die ihre Zeit als Mörder
     * "abgesessen" haben, werden aus der Mörder Liste genommen.
     */
    private void cleanUpMurderersList() {
        // Wir gehen die Liste per Iterator durch
        Iterator<Murderer> murdererIterator = murderers.iterator();
        
        // Abarbeiten der aktuellen Mörder
        while (murdererIterator.hasNext()) {
            Murderer currentMurderer = murdererIterator.next();
            
            // Wenn der aktuelle Mörder seine Zeit abgesessen hat wird er aus
            // der Liste der Mörder entfernt.
            if (currentMurderer.getTime() <= System.currentTimeMillis()) {
                murdererIterator.remove();
            }
        }
    }
    
    /**
     * Dieses Event benachrichtigt den HeadhunterZ Listener darüber, wenn ein
     * Mörder die Verbindung zum Server hergstellt hat.
     * 
     * @param eventData Enthält Daten über den Spieler
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    private void onMurdererConnected(PlayerJoinEvent eventData) {
        final Player joinedPlayer = eventData.getPlayer();
        
        // Prüfen, ob der verbundene Spieler ein Mörder ist
        if (isMurderer(joinedPlayer)) {
            // EVENT: onMurdererConnected
            for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                l.onMurdererConnected(getMurdererInfo(joinedPlayer));
            }
        }
    }
    
    /**
     * Dieses Event benachrichtigt den HeadhunterZ Listener darüber, wenn ein
     * Mörder die Verbindung zum Server getrennt hat.
     * 
     * @param eventData Enthält Daten über den Spieler
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    private void onMurdererDisconnected(PlayerQuitEvent eventData) {
        final Player disconnectedPlayer = eventData.getPlayer();
        
        // Prüfen, ob der Spieler ein Mörder ist
        if (isMurderer(disconnectedPlayer)) {
            // EVENT: onMurdererDisonnected
            for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                l.onMurdererDisconnected(getMurdererInfo(disconnectedPlayer));
            }
        }
    }
    
    /**
     * Dieses Event benachrichtigt den HeadhunterZ Listener darüber, wenn ein
     * Mörder einen Spieler angegriffen hat.
     * 
     * @param eventData Enthält Daten über den Angriff
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    private void onAttack(EntityDamageByEntityEvent eventData) {
        // Zuerst speichern wir den Angreifer temporär ab
        Entity damager = eventData.getDamager();
        
        // Dann prüfen wir, ob es sich um einen Spieler handelt
        if (damager instanceof Player) {
            Player pDamager = (Player) damager;
            
            // Schlussendlich müssen wir noch herausfinden, ob es sich bei diesem
            // Spieler um einen Mörder handelt. Wenn ja, wird das entsprechende
            // Event gefeuert.
            if (isMurderer(pDamager)) {
                // EVENT: onMurdererAttacksPlayer
                for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                    l.onMurdererAttacksPlayer(getMurdererInfo(pDamager), pDamager, eventData);
                }
            }
        }
    }
    
    /**
     * Das eigentliche Event, wenn ein Spieler getötet wurde.
     * 
     * Sammelt Daten über den Tod eines Spielers. Diese werden benötigt, wenn
     * auf den Killer ein Kopfgeld ausgesetzt werden soll.
     * @param eventData Enthält Daten über den Tod des Spielers
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    private void onKill(PlayerDeathEvent eventData) {
        // Zuerst prüfen wir, ob der Spieler durch einen anderen Spieler gestorben ist
        if (eventData.getEntityType() == EntityType.PLAYER) {
            // Sammeln der erforderlichen Informationen
            final Player killed = eventData.getEntity();
            final Player killer = killed.getKiller();
            final long killTime = System.currentTimeMillis();
            
            // Zuerst prüfen wir, ob es sich bei dem Killer um einen gelisteten
            // Mörder handelt. Wenn dies nicht der Fall ist wird geprüft, ob das
            // Opfer ein Mörder ist, was dazu führen würde, dass der Killer das
            // auf den Mörder ausgesetzte Kopfgeld erhält und der Mörder selbst
            // nicht weiter als Mörder gilt.
            if (!isMurderer(killer)) {
                // Wenn es sich bei dem Opfer um einen Mörder handelt, wird der
                // Killer nicht als Mörder verzeichnet werden.
                if (isMurderer(killed)) {
                    // Informationen über den Mörder
                    Murderer murdererInfo = getMurdererInfo(killed);
                    Murderer m = getMurdererInfo(killer);
                    
                    // Zahlt dem Spieler, der den Mörder gekillt hat, das ausgesetzte
                    // Kopfgeld aus.
                    this.pluginInstance.getEconomy().depositPlayer(killer.getName(), m.getEntireBounty());
                    
                    // Der Mörder wird aus der Liste genommen.
                    this.murderers.remove(murdererInfo);
                    
                    // EVENT: onMurdererKilled
                    for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                        l.onMurdererKilled(murdererInfo);
                    }
                } else {
                    // Die Informationen des Mörders werden in einem Objekt erfasst
                    final Murderer murderer = new Murderer(pluginInstance, killer);

                    // Fügt den aktuell gekillten Spieler als ersten hinzu
                    murderer.getKilled().put(killed, killTime);

                    // Setzt den Spieler auf die Liste der letzten Kills
                    murderers.add(murderer);

                    // EVENT: onPlayerGetsMurderer
                    for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                        l.onPlayerGetsMurderer(murderer);
                    }
                }
            } else {
                // Wenn der Spieler bereits ein Mörder ist wird der gekillte
                // Spieler zu dessen Kill-Liste hinzugefügt.
                getMurdererInfo(killer).getKilled().put(killed, killTime);
                
                // EVENT: onMurdererKilledPlayer
                for (MurdererListener l : pluginInstance.getRegisteredListeners()) {
                    l.onMurdererKilledPlayer(getMurdererInfo(killer), killer);
                }
            }
        }
    }
    
    /**
     * Prüft, ob der angegebene Spieler ein Mörder ist oder nicht.
     * @param player Der zu prüfende Spieler.
     * @return true, wenn es ein Mörder ist, false wenn nicht.
     */
    public boolean isMurderer(Player player) {
        // Wenn der Mörder gefunden werden kann ist es auch einer ;)
        return getMurdererInfo(player) != null;
    }
    
    /**
     * Gibt die Mörder Informationen anhand der Player Referenz zurück.
     * @param player Die Player Referenz
     * @return Die Mörder Informationen oder null, wenn keine existieren.
     */
    public Murderer getMurdererInfo(Player player) {
        // Die übergebene Player Referenz darf nicht null sein
        if (player == null) {
            throw new NullPointerException("Player cannot be null");
        }
        
        // Suchen nach der Mörder "Akte" im Mörder Verzeichnis.
        for (Murderer murderer : murderers) {
            if (murderer.getPlayer() == player) {
                // Wenn der aktuelle Mörder zum Spieler passt soll dieser
                // zurückgegeben werden.
                return murderer;
            }
        }
        
        // Wenn keine zugehörige "Akte" gefunden werden konnte wird null zurück
        // gegeben.
        return null;
    }
}