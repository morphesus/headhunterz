/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.maficraft.headhunterz.commands;

import de.maficraft.headhunterz.HeadhunterZ;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 * Verarbeitet die Commands des Plugins
 * @author Morph <admin@mds-tv.de>
 */
public class CommandDispatcher implements CommandExecutor, TabCompleter {
    /**
     * Referenz zum HeadhunterZ Plugin
     */
    private final HeadhunterZ pluginInstance;
    
    /**
     * Hält die registrierten Commands.
     * Der Index ist der Name des Subcommands im Lower-Case Format.
     */
    private final HashMap<String, CommandExecutor> registeredCommands = new HashMap<>();

    /**
     * Initialisiert den CommandDispatcher
     * @param pluginInstance Referenz zum HeadhunterZ Plugin
     */
    public CommandDispatcher(HeadhunterZ pluginInstance) {
        this.pluginInstance = pluginInstance;
    }

    /**
     * Wird gefeuert, wenn der hh Command vom Spieler oder der Konsole geschickt
     * wurde.
     * @param sender Der Sender des Commands
     * @param cmd Das Command Objekt
     * @param label Der Name des Commands
     * @param args Die Argumente des Commands
     * @return true, wenn der Command erfolgreich ausgeführt wurde, false wenn nicht
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        // Wenn der Master Command keine Argumente hat wird die Hilfe angezeigt
        if (args.length == 0) {
            
        }
        // Wenn Argumente vorhanden sind, gilt das erste als Label für den Subcommand.
        // Alle nachfolgenden Argumente werden als Array an den Subcommand weitergeleitet.
        else {
            String subCmdName = args[0];
            String[] subCmdArgs = new String[args.length-1];
            
            // Kopieren der Argumente in das Array für den Subcommand
            System.arraycopy(args, 1, subCmdArgs, 0, args.length - 1);
            
            // Wir versuchen uns den CommandExecutor zu holen
            CommandExecutor cmdExec = this.registeredCommands.get(subCmdName.toLowerCase());
            
            // Hier prüfen wir, ob es den Command eigentlich gibt.
            // Wenn er existiert, führen wir ihn mit den entsprechenden Parametern aus.
            if (cmdExec != null) {
                return cmdExec.onCommand(sender, cmd, subCmdName, subCmdArgs);
            }
            // Wenn der Command nicht existiert wird eine Exception geworfen.
            else {
                throw new IllegalArgumentException("Unknown sub-command: " + subCmdName);
            }
        }
        
        return true;
    }

    /**
     * Wird gefeuert, wenn der Nutzer die Auto-Vervollständigung benutzt
     * @param sender Der Sender des Commands
     * @param cmd Das Command Objekt
     * @param label Der Name des Commands
     * @param args Die Argumente des Commands
     * @return 
     */
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        // TODO Autovervollständigung implementieren
        return new ArrayList<>();
    }
    
    /**
     * Registriert einen Command anhand seiner Klasse.
     * @param subCommandName Name des Subcommands
     * @param cmdClass Die Klasse des Commands.
     */
    public void registerCommand(String subCommandName, Class<CommandExecutor> cmdClass) {
        // Zuerst prüfen wir, ob die cmdClass auch nicht null ist.
        // Wenn sie es nicht ist, instanziieren wir sie und fügen sie der Liste
        // der registrierten Commands hinzu.
        if (cmdClass != null) {
            try {
                CommandExecutor cmdExec = cmdClass.newInstance();
                this.registeredCommands.put(subCommandName, cmdExec);
            } catch (InstantiationException | IllegalAccessException ex) {
                pluginInstance.getLogger().log(Level.SEVERE, null, ex);
            }
        }
        // Wenn der übergebene cmdClass Parameter null ist, wird eine entsprechende
        // Exception geworfen.
        else {
            throw new NullPointerException("The command class cannot be null");
        }
    }
}
